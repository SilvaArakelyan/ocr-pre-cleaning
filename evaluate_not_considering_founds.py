import json
import os
from innolytiq.document.tag import ExtractionTag
from innolytiq.document.page import create_dummy_page
from pathlib import Path
from utils import word_to_extraction_tag, matches
from innolytiq.common.utils import ConfusionMatrix


def evaluate_all(act_ocr_folder_path: str, pred_ocr_folder_path: str, founds_dict: dict):
    """ Input paths of gt and pred ocr.json's folders and founds_dict. Assumes names are image_name + ocr (GT)
    and image_name +ocr_processed (pred) correspondingly. founds_dict is dictionary containing locations where
    words were already detected previously
    - finds matches in pred and act
    - calls evaluate on found match (gt and pred)"""
    tp, tn, fp, fn = 0, 0, 0, 0
    for act_ocr_file_name in os.listdir(act_ocr_folder_path):
        act_ocr_path = os.path.join(act_ocr_folder_path, act_ocr_file_name)
        pred_ocr_path = os.path.join(pred_ocr_folder_path, f"{act_ocr_file_name.split('.json')[0]}_processed.json")
        with open(act_ocr_path) as json_file:
            gt_ocr_json = json.load(json_file)
        if Path(pred_ocr_path).is_file():
            with open(pred_ocr_path) as json_file:
                pred_ocr_json = json.load(json_file)
                per_conf_matrix = evaluate(gt_ocr_json, pred_ocr_json,
                                           already_found_tps=founds_dict[act_ocr_file_name.split('_ocr')[0]])
                tp += per_conf_matrix.TP
                fp += per_conf_matrix.FP
                fn += per_conf_matrix.FN
                print(f'DOCUMENT: {act_ocr_file_name.split("_ocr")[0]} ___ {per_conf_matrix}')
    conf_matrix = ConfusionMatrix(TP=tp, TN=tn, FP=fp, FN=fn)
    results = conf_matrix.compute_metrics()
    print(results)
    return results


def evaluate(ocr_json_gt: dict, ocr_json_pred: dict, already_found_tps: list = None) -> ConfusionMatrix:
    matched_tps = []
    gt_image_width = float(ocr_json_gt['page']['width'])
    gt_image_height = float(ocr_json_gt['page']['height'])
    pred_image_width = ocr_json_pred['image']['width']
    pred_image_height = ocr_json_pred['image']['height']
    ocr_data_act = ocr_json_gt['data']
    ocr_data_pred = ocr_json_pred['data']
    ocr_data_act_cleaned = [word for word in ocr_data_act if word['value'].strip()]
    ocr_data_pred_cleaned = [word for word in ocr_data_pred if word['value'].strip()]
    tp, tn, fp, fn = 0, 0, 0, 0
    found_not_needed = 0
    for pred_word in ocr_data_pred_cleaned:
        # for counting fp's, do not consider predictions in already found areas
        pred_tag = word_to_extraction_tag(pred_word, image_width=pred_image_width, image_height=pred_image_height)
        if any([matches(ExtractionTag(
                left=already_found_tp[0],
                right=already_found_tp[1],
                top=already_found_tp[2],
                bottom=already_found_tp[3],
                page=create_dummy_page(),
                raw_value='',
                raw_ocr_value=''), pred_tag) for already_found_tp in already_found_tps]):
            found_not_needed += 1

    already_found = 0
    for act_word in ocr_data_act_cleaned:
        act_tag = word_to_extraction_tag(act_word, image_width=gt_image_width, image_height=gt_image_height)
        # for counting fn's, for not counting already found actuals
        if any([matches(ExtractionTag(
                left=already_found_tp[0],
                right=already_found_tp[1],
                top=already_found_tp[2],
                bottom=already_found_tp[3],
                page=create_dummy_page(),
                raw_value='',
                raw_ocr_value=''), act_tag, threshold=0.9) for already_found_tp in already_found_tps]):
            already_found += 1
            continue
        for pred_word in ocr_data_pred_cleaned:
            pred_tag = word_to_extraction_tag(pred_word, image_width=pred_image_width, image_height=pred_image_height)
            if matches(act_tag, pred_tag) and act_tag.raw_ocr_value == pred_tag.raw_ocr_value:
                matched_tps.append(act_tag)
                # print(act_tag.raw_ocr_value)
                tp += 1
                break
    fp = len(ocr_data_pred_cleaned) - tp - found_not_needed
    fn = len(ocr_data_act_cleaned) - tp - already_found
    conf_matrix = ConfusionMatrix(TP=tp, FP=fp, FN=fn, TN=tn)
    return conf_matrix


if __name__ == '__main__':
    gt_path = ''
    pred_path = ''
    with open('') as outfile:
        found_words_dict = json.load(outfile)
    results = evaluate_all(gt_path, pred_path, found_words_dict)

    # for evaluating single actual - predicted OCRs
    with open('/path/img_name_ocr.json') as json_file:
        gt_ocr_json = json.load(json_file)
    with open('/path/img_name_ocr_processed.json') as json_file:
        pred_ocr_json = json.load(json_file)
    print(evaluate(gt_ocr_json, pred_ocr_json, already_found_tps=found_words_dict['img_name']))

