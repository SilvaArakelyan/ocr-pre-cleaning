import cv2
import os


def apply_otsus_thresholding(source_path: str, output_path: str, thresh_type=cv2.THRESH_BINARY):
    """ Apply Otsu's thresholding method, assumes bimodal distribution
    :param source_path: directory path of images to be processed
    :param output_path: directory path for saving the processed results
    :param thresh_type: thresholding type, choices are cv2.THRESH_BINARY, cv2.THRESH_BINARY_INV, cv2.THRESH_TRUNC,
            cv2.THRESH_TOZERO, cv2.THRESH_TOZERO_INV
     """
    image_names = os.listdir(source_path)
    for image_name in image_names:
        image_path = os.path.join(source_path, image_name)
        img = cv2.imread(image_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gray, 0, 255, thresh_type | cv2.THRESH_OTSU)
        cv2.imwrite(os.path.join(output_path, f'{image_name}'), thresh)


if __name__ == '__main__':
    apply_otsus_thresholding(source_path='',
                             output_path='',
                             thresh_type=cv2.THRESH_TRUNC)
