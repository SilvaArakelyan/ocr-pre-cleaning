import cv2
import os
import shutil


def tune_adaptive_thresholding(source_path: str,
                               output_path: str,
                               blocksize_grid: list,
                               C_grid: list,
                               adaptive_thresh_type=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                               thresh_type=cv2.THRESH_BINARY):

    """ Tune parameters blocksize and C of adaptive thresholding
    :param source_path: directory path of images to be processed
    :param output_path: directory path for saving the processed results
    :param adaptive_thresh_type: adaptive thresholding type, either cv2.ADAPTIVE_THRESH_GAUSSIAN_C
            or cv2.ADAPTIVE_THRESH_MEAN_C
    :param thresh_type: thresholding type
    :param blocksize_grid: grid for blocksize
    :param C_grid: grid for C
    """
    image_names = os.listdir(source_path)

    for blocksize in blocksize_grid:
        for c in C_grid:
            new_dir_path = f'{output_path}/{blocksize}_{c}'
            new_before_after_path = f'{output_path}/{blocksize}_{c}_before_after'
            os.makedirs(new_dir_path)
            os.makedirs(new_before_after_path)
            for image_name in image_names:
                img_path = os.path.join(source_path, image_name)
                img = cv2.imread(img_path)
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                # blurred = cv2.GaussianBlur(gray, (5, 5), 0)
                thresh = cv2.adaptiveThreshold(gray, 255,
                                               adaptiveMethod=adaptive_thresh_type,
                                               thresholdType=thresh_type,
                                               blockSize=blocksize,
                                               C=c)
                cv2.imwrite(os.path.join(new_dir_path, f'{image_name}'), thresh)
                cv2.imwrite(os.path.join(new_before_after_path, f'{image_name.split("jpeg")[0]}_after.jpeg'), thresh)
                shutil.copy(img_path, f'{new_before_after_path}/{image_name.split("jpeg")[0]}_before.jpeg')
            print(f'Done:{blocksize}_{c}')


if __name__ == '__main__':
    tune_adaptive_thresholding(source_path='',
                               output_path='',
                               blocksize_grid=list(range(37, 40, 2)),
                               C_grid=[1, 2, 3])
