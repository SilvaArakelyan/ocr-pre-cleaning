import cv2
import os


def apply_histogram_equalization(source_path: str, output_path: str, clipLimit: float = 2.0,
                                 tileGridSize: tuple = (30, 30)):
    """ Do contrast limited adaptive histogram equalization
    :param source_path: directory path of images to be processed
    :param output_path: directory path for saving the processed results
    :param tileGridSize:
    :param clipLimit:
    """

    images_path_list = os.listdir(source_path)
    for image_name in images_path_list:
        img_path = os.path.join(source_path, image_name)
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=clipLimit, tileGridSize=tileGridSize)
        cl_img = clahe.apply(gray)
        cv2.imwrite(os.path.join(output_path, f'{image_name}'), cl_img)


if __name__ == '__main__':
    apply_histogram_equalization(source_path='',
                                 output_path='')
