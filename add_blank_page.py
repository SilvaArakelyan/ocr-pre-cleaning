import PyPDF2
import os


def add_blank_page(input_folder_path: str, out_folder_path: str):
    """ Adding blank page in the end of pdfs in specified input directory and save in specified output directory
    """
    for doc_name in os.listdir(input_folder_path):
        doc = open(os.path.join(input_folder_path, doc_name), 'rb')
        pdf = PyPDF2.PdfFileReader(doc)
        out_pdf = PyPDF2.PdfFileWriter()
        out_pdf.appendPagesFromReader(pdf)
        out_pdf.addBlankPage()
        out_stream = open(f'{out_folder_path}/{doc_name}', 'wb')
        out_pdf.write(out_stream)
        out_stream.close()
        doc.close()


if __name__ == '__main__':
    add_blank_page(input_folder_path='',
                   out_folder_path='')
