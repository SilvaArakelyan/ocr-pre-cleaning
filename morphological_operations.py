import os
import cv2

""" Do dilation, erosion, opening and closing with different kernels. Specify source_path and output_path
    source_path: directory path of images to be processed
    output_path: directory path for saving the processed results
"""
source_path = ''
output_path = ''
images_path_list = os.listdir(source_path)
for image_name in images_path_list:
    img_path = os.path.join(source_path, image_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # can define kernel by hand or use cv2.getStructuringElement
    # kernel = np.ones((1, 1), np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    # kernel = cv2.getStructuringElement(cv2.MARKER_SQUARE, (3, 3))
    # img = cv2.dilate(img, kernel, iterations=1)
    # img = cv2.erode(img, kernel, iterations=1)
    opening = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)
    # closing = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel)
    # blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, kernel)
    cv2.imwrite(os.path.join(output_path, f'{image_name}'), opening)
    # saving before after images
    # cv2.imwrite(f'{output}/{img_name.split(".jpeg")[0]}_after.jpeg', opening)
    # shutil.copy(img_path, f'{output}/{img_name.split(".jpeg")[0]}_before.jpeg')
