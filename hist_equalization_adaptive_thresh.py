import cv2
import os

""" Histogram equalization + adaptive thresholding """
source_path = ''
output_path = ''
images_path_list = os.listdir(source_path)
for image_name in images_path_list:
    img_path = os.path.join(source_path, image_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=1, tileGridSize=(25, 25))
    cl_img = clahe.apply(gray)
    thresh = cv2.adaptiveThreshold(cl_img, 255,
                                   adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                   thresholdType=cv2.THRESH_BINARY,
                                   blockSize=25,
                                   C=25)
    cv2.imwrite(os.path.join(output_path, f'{image_name}'), thresh)
