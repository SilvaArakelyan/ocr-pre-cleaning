import os
import cv2

""" Blurring + Adaptive thresholding """
path = ''
imgs = os.listdir('')
new_before_after_path = ''
for image_name in imgs:
    img_path = os.path.join(path, image_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, blockSize=3, C=3)
    cv2.imwrite(os.path.join(new_before_after_path, f'{image_name}'), thresh)
    # cv2.imwrite(os.path.join(new_before_after_path, f'{image_name.split("jpeg")[0]}_after.jpeg'), thresh)
    # shutil.copy(img_path, f'{new_before_after_path}/{image_name.split("jpeg")[0]}_before.jpeg')
