import os
import cv2


def apply_simple_blurring(source_path: str, output_path: str, ksize: tuple = (9, 9)):
    """ Simple blurring
    :param source_path: directory path of images to be processed
    :param output_path: directory path to save the pre processed results
    :param ksize: kernel size """
    img_names = os.listdir(source_path)
    for img_name in img_names:
        img_path = os.path.join(source_path, img_name)
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blurred = cv2.blur(gray, ksize=ksize)
        cv2.imwrite(os.path.join(output_path, img_name), blurred)


def apply_gaussian_blurring(source_path: str, output_path: str,
                            ksize: tuple = (5, 5),
                            sigmaX: float = 0,
                            sigmaY: float = 0.1):
    """ Gaussian blurring
        :param source_path: directory path of images to be processed
        :param output_path: directory path to save the pre processed results
        :param ksize: kernel size, width and height of kernel, should be odd and positive
        :param sigmaX: standard deviation in the X direction
        :param sigmaY: standard deviation in the Y direction
        """
    img_names = os.listdir(source_path)
    for img_name in img_names:
        img_path = os.path.join(source_path, img_name)
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, ksize=ksize, sigmaX=sigmaX, sigmaY=sigmaY)
        cv2.imwrite(os.path.join(output_path, img_name), blurred)


def apply_median_blurring(source_path: str, output_path: str, ksize: int = 3):
    """ Median blurring
        :param source_path: directory path of images to be processed
        :param output_path: directory path to save the pre processed results
        :param ksize: kernel size"""
    img_names = os.listdir(source_path)
    for img_name in img_names:
        img_path = os.path.join(source_path, img_name)
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blurred = cv2.medianBlur(gray, ksize=ksize)
        cv2.imwrite(os.path.join(output_path, img_name), blurred)
