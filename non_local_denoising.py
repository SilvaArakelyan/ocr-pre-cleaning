import os
import cv2


def apply_fast_denoise(source_path: str, output_path: str, templateWindowSize: int = 5, searchWindowSize: int = 21):
    """ Non-local Means Denoising
        :param source_path: directory path of images to be processed
        :param output_path: directory path for saving the processed results
        :param templateWindowSize:
        :param searchWindowSize"""
    imgs = os.listdir(source_path)
    for image_name in imgs:
        img_path = os.path.join(source_path, image_name)
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # apply thresholding before denoising
        # thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,
        #                                blockSize=25, C=25)
        dst = cv2.fastNlMeansDenoising(gray, templateWindowSize=templateWindowSize, searchWindowSize=searchWindowSize)
        cv2.imwrite(os.path.join(output_path, f'{image_name}'), dst)


if __name__ == '__main__':
    apply_fast_denoise(source_path='',
                       output_path='')
