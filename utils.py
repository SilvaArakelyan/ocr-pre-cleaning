from innolytiq.document.tag import ExtractionTag
from innolytiq.document.page import create_dummy_page


def matches(act_tag: ExtractionTag, pred_tag: ExtractionTag, threshold: float = 0.6) -> bool:
    """ Detects if there is a match between two extraction tags having the same page number. Returns true if
    intersection is greater than the threshold"""
    return act_tag.page.page_number == pred_tag.page.page_number and (
            act_tag & pred_tag) / min(act_tag, pred_tag, key=lambda x: x.area).area >= threshold


def word_to_extraction_tag(word: dict, image_width, image_height) -> ExtractionTag:
    return ExtractionTag(left=float(word['x']) / image_width * 100,
                         right=(float(word['x']) + float(word['w'])) / image_width * 100,
                         top=float(word['y']) / image_height * 100,
                         bottom=(float(word['y']) + float(word['h'])) / image_height * 100,
                         page=create_dummy_page(),
                         raw_value=word['value'],
                         raw_ocr_value=word['value'])
