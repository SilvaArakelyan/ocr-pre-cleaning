import os
import cv2

''' Median Blurring + Adaptive Thresholding'''
path = ''
output_path = ''
img_names = os.listdir(path)
for img_name in img_names:
    img_path = os.path.join(path, img_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.medianBlur(gray, ksize=3)
    thresh = cv2.adaptiveThreshold(blurred, 255,
                                   adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                   thresholdType=cv2.THRESH_BINARY,
                                   blockSize=25,
                                   C=25)
    cv2.imwrite(os.path.join(output_path, img_name), thresh)
