import cv2
from cv2 import dnn_superres
import os

imput_path = ''
output_path = ''
sr = dnn_superres.DnnSuperResImpl_create()
# for using the models you must set-up and give the path
path = "TF-ESPCN-master/export/ESPCN_x4.pb"
for img_name in os.listdir(imput_path):
    img = cv2.imread(os.path.join(imput_path, img_name))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # apply blurring before scaling
    # blurred = cv2.medianBlur(gray, 3)
    sr.readModel(path)
    sr.setModel("espcn", 4)
    result = sr.upsample(gray)
    # apply thresholding
    # thresh = cv2.adaptiveThreshold(result, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, blockSize=25, C=25)
    # dst = cv2.fastNlMeansDenoising(result)
    cv2.imwrite(os.path.join(output_path, img_name), result)
