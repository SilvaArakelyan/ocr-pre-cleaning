import os
import cv2
import shutil


""" Experimenting with watermarked reports"""
path = ''
imgs = os.listdir(path)
new_before_after_path = ''

for image_name in imgs:
    img_path = os.path.join(path, image_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # dst = cv2.fastNlMeansDenoising(gray)
    # blurred = cv2.medianBlur(gray, 3)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))
    # result = cv2.erode(gray, kernel)
    close = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel)
    # open = cv2.morphologyEx(blurred, cv2.MORPH_OPEN, kernel)
    # thresh = cv2.threshold(blurred, 100 , 255, cv2.THRESH_BINARY)
    # thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 9, 2)
    # ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    cv2.imwrite(os.path.join(new_before_after_path, f'{image_name.split("jpeg")[0]}_after.jpeg'), close)
    shutil.copy(img_path, f'{new_before_after_path}/{image_name.split("jpeg")[0]}_before.jpeg')
