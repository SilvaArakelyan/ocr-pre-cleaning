import cv2
import os

""" Simple resize with different interpolation types"""
source_path = ''
output_path = ''
interpolation_type = cv2.INTER_AREA
# interpolation_type = cv2.INTER_CUBIC
# interpolation_type= cv2.INTER_LANCZOS4
# interpolation_type = cv2.INTER_NEAREST
# interpolation_type = cv2.INTER_LINEAR
img_width = 1700  # instead of giving by hand it is better to calculate aspect ratio
img_height = 2200
images_names = os.listdir(source_path)
for image_name in images_names:
    img_path = os.path.join(source_path, image_name)
    img = cv2.imread(img_path)
    resized = cv2.resize(img, (img_width, img_height), interpolation=interpolation_type)
    cv2.imwrite(os.path.join(output_path, f'{image_name}'), resized)
