import cv2
import os

""" Test Median blurring + adaptive 2 types on single image """
source_path = ''
output_path = ''
images_path_list = os.listdir(source_path)
# Adaptive Thresholding
non_adaptive_types = [cv2.ADAPTIVE_THRESH_MEAN_C, cv2.ADAPTIVE_THRESH_GAUSSIAN_C]
for image_name in images_path_list:
    img_path = os.path.join(source_path, image_name)
    for idx, ad_type in enumerate(non_adaptive_types, start=1):
        # grayscale image | maxValue | adaptiveMethod | thresholdType | blockSize | C
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blurred = cv2.medianBlur(gray, ksize=5)
        thresh = cv2.adaptiveThreshold(blurred, 255,
                                       adaptiveMethod=ad_type,
                                       thresholdType=cv2.THRESH_BINARY,
                                       blockSize=5, C=2)
        cv2.imwrite(os.path.join(output_path, f'{image_name.split(".jpeg")[0]}__{idx}.jpeg'), thresh)
