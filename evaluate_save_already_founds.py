import json
import os
from pathlib import Path
from utils import word_to_extraction_tag, matches
from innolytiq.common.utils import ConfusionMatrix


def evaluate_all(act_ocr_folder_path: str, pred_ocr_folder_path: str):
    """ Input paths of gt and pred ocr.json's folders. Assumes names are image_name + ocr (GT)
    and image_name +ocr_processed (pred) correspondingly
    - finds matches in pred and act
    - calls evaluate on found match (gt and pred)
    - for each match (act, pred) saves actual locations which were found"""
    tp, tn, fp, fn = 0, 0, 0, 0
    already_found_words_loc_dict = {}
    for act_ocr_file_name in os.listdir(act_ocr_folder_path):
        act_ocr_path = os.path.join(act_ocr_folder_path, act_ocr_file_name)
        pred_ocr_path = os.path.join(pred_ocr_folder_path, f"{act_ocr_file_name.split('.json')[0]}_processed.json")
        with open(act_ocr_path) as json_file:
            gt_ocr_json = json.load(json_file)
        if Path(pred_ocr_path).is_file():
            with open(pred_ocr_path) as json_file:
                pred_ocr_json = json.load(json_file)
                per_conf_matrix, matched_tp = evaluate(gt_ocr_json, pred_ocr_json)
                already_found_words_loc_dict[act_ocr_file_name.split('_ocr')[0]] = matched_tp
                tp += per_conf_matrix.TP
                fp += per_conf_matrix.FP
                fn += per_conf_matrix.FN
                print(f'DOCUMENT: {act_ocr_file_name.split("_ocr")[0]} ___ {per_conf_matrix}')
    conf_matrix = ConfusionMatrix(TP=tp, TN=tn, FP=fp, FN=fn)
    results = conf_matrix.compute_metrics()
    print(results)
    return results, already_found_words_loc_dict


def evaluate(ocr_json_gt: dict, ocr_json_pred: dict):
    matched_tps = []
    gt_image_width = float(ocr_json_gt['page']['width'])
    gt_image_height = float(ocr_json_gt['page']['height'])
    pred_image_width = ocr_json_pred['image']['width']
    pred_image_height = ocr_json_pred['image']['height']
    ocr_data_act = ocr_json_gt['data']
    ocr_data_pred = ocr_json_pred['data']
    ocr_data_act_cleaned = [word for word in ocr_data_act if word['value'].strip()]
    ocr_data_pred_cleaned = [word for word in ocr_data_pred if word['value'].strip()]
    tp, tn, fp, fn = 0, 0, 0, 0
    for act_word in ocr_data_act_cleaned:
        act_tag = word_to_extraction_tag(act_word, image_width=gt_image_width, image_height=gt_image_height)
        for pred_word in ocr_data_pred_cleaned:
            pred_tag = word_to_extraction_tag(pred_word, image_width=pred_image_width, image_height=pred_image_height)
            if matches(act_tag, pred_tag) and act_tag.raw_ocr_value == pred_tag.raw_ocr_value:
                matched_tps.append(list([act_tag.left, act_tag.right, act_tag.top, act_tag.bottom]))
                tp += 1
                break
    fp = len(ocr_data_pred_cleaned) - tp
    fn = len(ocr_data_act_cleaned) - tp
    conf_matrix = ConfusionMatrix(TP=tp, FP=fp, FN=fn, TN=tn)
    return conf_matrix, matched_tps


if __name__ == '__main__':
    gt_path = ''
    tess_pred = ''
    results, founds = evaluate_all(gt_path, tess_pred)
    with open('', 'w') as outfile:
        json.dump(founds, outfile)
