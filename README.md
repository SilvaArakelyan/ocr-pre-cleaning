# Testing OCR pre-cleaning methods

- install requirements
- do the pre-processing   
- for evaluation run OCR service on pre-processed images -> generate OCRs, 
  then evaluate by comparing to ground truth OCR
- for applying super resolution, download pre-trained models, go to: https://learnopencv.com/super-resolution-in-opencv/  
