import os
import cv2

""" Simple thresholding with binary inverse type + Gaussian blurring"""

path = ''
output_path = ''
new_before_after_path = ''
img_names = os.listdir(path)
for img_name in img_names:
    img_path = os.path.join(path, img_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.threshold(blurred, 105, 255, cv2.THRESH_BINARY_INV)[1]
    thresh = 255 - thresh
    cv2.imwrite(os.path.join(output_path, img_name), thresh)
    # save before after images
    # cv2.imwrite(os.path.join(new_before_after_path, f'{img_name.split(".jpeg")[0]}_after.jpeg'), thresh)
    # shutil.copy(img_path, f'{new_before_after_path}/{img_name.split(".jpeg")[0]}_before.jpeg')
