import os
import cv2

""" Experimenting on watermarked reports"""
source_path = ''
imgs = os.listdir(source_path)
new_before_after_path = ''
for image_name in imgs:
    img_path = os.path.join(source_path, image_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    # Morph open to remove noise
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=1)

    # Invert and apply slight Gaussian blur
    result = 255 - opening
    result = cv2.GaussianBlur(result, (3, 3), 0)
    cv2.imwrite(os.path.join(new_before_after_path, f'{image_name}'), result)
    # cv2.imwrite(os.path.join(new_before_after_path, f'{image_name.split("jpeg")[0]}_after.jpeg'), result)
    # shutil.copy(img_path, f'{new_before_after_path}/{image_name.split("jpeg")[0]}_before.jpeg')
