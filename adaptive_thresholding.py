import cv2
import os


def apply_adaptive_thresholding(source_path: str,
                                output_path: str,
                                blocksize: int = 25,
                                c: int = 25,
                                adaptive_thresh_type=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                thresh_type=cv2.THRESH_BINARY):
    """ Adaptive Thresholding
    :param source_path: directory path of images to be processed
    :param output_path: directory path for saving the processed results
    :param blocksize: size of the neighbourhood area, should be odd number
    :param c: constant that is subtracted from the mean or weighted sum of the neighbourhood pixels
    :param adaptive_thresh_type: adaptive thresholding type, either cv2.ADAPTIVE_THRESH_GAUSSIAN_C
            or cv2.ADAPTIVE_THRESH_MEAN_C
    :param thresh_type: thresholding type
        """
    images_path_list = os.listdir(source_path)
    for image_name in images_path_list:
        img_path = os.path.join(source_path, image_name)
        img = cv2.imread(img_path)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        thresh = cv2.adaptiveThreshold(gray, 255,
                                       adaptiveMethod=adaptive_thresh_type,
                                       thresholdType=thresh_type,
                                       blockSize=blocksize,
                                       C=c)
        cv2.imwrite(os.path.join(output_path, f'{image_name}'), thresh)


if __name__ == '__main__':
    apply_adaptive_thresholding(source_path='',
                                output_path='',
                                adaptive_thresh_type=cv2.ADAPTIVE_THRESH_MEAN_C,
                                thresh_type=cv2.THRESH_BINARY_INV,
                                blocksize=3,
                                c=2)
