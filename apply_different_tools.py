import os
import cv2

""" Apply different tools, experiment by interchanging the order, giving different parameter values"""
path = ''
output_path = ''
adaptive_thresh_type = cv2.ADAPTIVE_THRESH_GAUSSIAN_C
# adaptive_thresh_type = cv2.ADAPTIVE_THRESH_MEAN_C
thresh_type = cv2.THRESH_BINARY
# thresh_type = cv2.THRESH_BINARY_INV
# thresh_type= cv2.THRESH_TRUNC
# thresh_type = cv2.THRESH_TOZERO
# thresh_type = cv2.THRESH_TOZERO_INV
img_names = os.listdir(path)
for img_name in img_names:
    img_path = os.path.join(path, img_name)
    img = cv2.imread(img_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # can add applying blurring or fastdenoise then giving result as input to thresholding
    blurred = cv2.medianBlur(gray, ksize=3)
    # blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    # dst = cv2.fastNlMeansDenoising(gray, templateWindowSize=5, searchWindowSize=19)
    thresh = cv2.adaptiveThreshold(blurred, 255,
                                   adaptiveMethod=adaptive_thresh_type,
                                   thresholdType=thresh_type,
                                   blockSize=25,
                                   C=25)
    # apply histogram equalization
    # clahe = cv2.createCLAHE(clipLimit=3, tileGridSize=(25, 25))
    # cl_img = clahe.apply(thresh)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    # closing = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel)
    cv2.imwrite(os.path.join(output_path, img_name), opening)
